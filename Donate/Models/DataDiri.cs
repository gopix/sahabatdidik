﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donate.Models
{
    [Table("DataDiri")]
    public class DataDiri
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        [Required(ErrorMessage = "Nama harus diisi")]
        public string Nama { get; set; }
        public string GelarDepan { get; set; }
        public string GelarBelakang { get; set; }
        public string TempatLahir { get; set; }

        public Nullable<DateTime> TglLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }

        [Required(ErrorMessage = "Email harus diisi")]
        [EmailAddress(ErrorMessage = "Email tidak valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "No telp harus diisi")]
        public string NoTelp { get; set; }

        public string Kewarganegaraan { get; set; }
        public string NoKtp { get; set; }
        public bool IsDonatur { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime LastModified { get; set; }

        [ForeignKey("Agama")]
        public Nullable<int> AgamaId { get; set; }
        public virtual Agama Agama { get; set; }

        [ForeignKey("StatusNikah")]
        public Nullable<int> StatusNikahId { get; set; }
        public virtual StatusNikah StatusNikah { get; set; }

        [ForeignKey("GolonganDarah")]
        public Nullable<int> GolonganDarahId { get; set; }
        public virtual GolonganDarah GolonganDarah { get; set; }

        public List<PendidikanFormal> PendidikanFormals { get; set; }
    }
}