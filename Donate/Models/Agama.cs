﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donate.Models
{
    [Table("Agama")]
    public class Agama
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Nama { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime LastModified { get; set; }

        public virtual List<DataDiri> DataDiris { get; set; }
    }
}