﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Donate.Models
{
    [Table("Gallery")]
    public class Gallery
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Display(Name = "Keterangan")]
        [Required(ErrorMessage = "Keterangan harus diisi.")]
        public string Description { get; set; }

        public string ImageName { get; set; }

        [NotMapped]
        [Display(Name = "Gambar")]
        //[Required(ErrorMessage = "Gambar harus dipilih")]
        //[ImageFile(Width = 790, Height = 400, ErrorMessage = "Gambar yang diijinkan minimal 790x400")]
        //[FileTypes("jpg,jpeg,png")]
        public HttpPostedFileBase Image { get; set; }

        public DateTime LastModified { get; set; }

        [Display(Name = "Tampil")]
        public bool IsActive { get; set; }
    }
}