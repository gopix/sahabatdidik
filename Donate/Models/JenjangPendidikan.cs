﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donate.Models
{
    [Table("JenjangPendidikan")]
    public class JenjangPendidikan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string KodeJenjang { get; set; }
        public string Jenjang { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime LastModified { get; set; }

        public List<PendidikanFormal> PendidikanFormals { get; set; }
    }
}