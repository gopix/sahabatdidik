﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Donate.Models
{
    [Table("Rekening")]
    public class Rekening
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Bank harus diisi.")]
        [Display(Name = "Bank")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "No. Rekening harus diisi.")]
        [Display(Name = "No. Rekening")]
        public string Nomor { get; set; }

        [Required(ErrorMessage = "Atas Nama Rekening harus diisi.")]
        [Display(Name = "Atas Nama Rekening")]
        public string AtasNama { get; set; }

        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Last Modified")]
        public DateTime LastModified { get; set; }
    }
}