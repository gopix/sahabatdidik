﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Donate.Models
{
    [Table("Berita")]
    public class Berita
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Display(Name = "Judul")]
        [Required(ErrorMessage = "Judul harus diisi.")]
        public string Title { get; set; }

        [Display(Name = "Ikhtisar")]
        [Required(ErrorMessage = "Ikhtisar harus isi")]
        [StringLength(100, ErrorMessage = "Ikhtisar Maksimal 100 karakter.")]
        public string Ikhtisar { get; set; }

        [Display(Name = "Isi")]
        [AllowHtml]
        public string Content { get; set; }

        public string ImageName { get; set; }

        [NotMapped]
        [Display(Name = "Gambar")]
        //[Required(ErrorMessage = "Gambar harus dipilih")]
        //[ImageFile(Width = 790, Height = 400, ErrorMessage = "Gambar yang diijinkan minimal 790x400")]
        //[FileTypes("jpg,jpeg,png")]
        public HttpPostedFileBase Image { get; set; }

        public DateTime LastModified { get; set; }

        [Display(Name = "Tampil")]
        public bool IsActive { get; set; }
    }
}