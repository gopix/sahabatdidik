﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donate.Models
{
    [Table("PendidikanFormal")]
    public class PendidikanFormal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int DataDiriId { get; set; }
        public int JenjangPendidikanId { get; set; }
        public string NamaSekolah { get; set; }
        public string Jurusan { get; set; }
        public string Konsentrasi { get; set; }
        public string Akreditasi { get; set; }
        public int TahunMasuk { get; set; }
        public int TahunLulus { get; set; }
        public string AlamatSekolah { get; set; }
        public string NilaiAkhir { get; set; }
        public string NoIjazah { get; set; }
        public string NoInduk { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime LastModified { get; set; }

        public virtual DataDiri DataDiri { get; set; }
        public virtual JenjangPendidikan JenjangPendidikan { get; set; }
    }
}