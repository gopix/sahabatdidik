﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Donate.Models
{
    public partial class ImageFileAttribute : ValidationAttribute
    {
        public int Width;
        public int Height;

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            var image = new Bitmap(file.InputStream);
            if (image == null)
                return true;

            if (image.Width < Width)
                return false;

            if (image.Height < Height)
                return false;

            return true;
        }
    }

    public class FileTypesAttribute : ValidationAttribute
    {
        private readonly List<string> _types;

        public FileTypesAttribute(string types)
        {
            _types = types.Split(',').ToList();
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;

            var fileExt = System.IO
                                .Path
                                .GetExtension((value as
                                         HttpPostedFileBase).FileName).Substring(1);
            return _types.Contains(fileExt, StringComparer.OrdinalIgnoreCase);
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("hanya diijinkan file gambar dengan type {0}.", String.Join(", ", _types));
        }
    }
}