﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Donate.Models
{
    [Table("Mutasi")]
    public class Mutasi
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public decimal Saldo { get; set; }

        public decimal Debit { get; set; }

        public decimal Kredit { get; set; }

        [Required(ErrorMessage = "Keterangan harus diisi.")]
        public string Keterangan { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime LastModified { get; set; }

        [Display(Name = "Donatur")]
        [ForeignKey("Donatur")]
        public Nullable<int> DonaturId { get; set; }
        public virtual DataDiri Donatur { get; set; }

        [ForeignKey("Siswa")]
        public Nullable<int> SiswaId { get; set; }
        public virtual DataDiri Siswa { get; set; }

        [ForeignKey("Rekening")]
        public int RekeningId { get; set; }
        public virtual Rekening Rekening { get; set; }
    }
}