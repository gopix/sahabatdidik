﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Donate.Models
{
    [Table("Pesan")]
    public class Pesan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string GuestName { get; set; }
        public string IsiPesan { get; set; }
        public bool IsReaded { get; set; }
        public DateTime LastModified { get; set; }

        [Display(Name = "Donatur")]
        [ForeignKey("Donatur")]
        public Nullable<int> DonaturId { get; set; }
        public virtual DataDiri Donatur { get; set; }
    }
}