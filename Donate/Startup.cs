﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Donate.Startup))]
namespace Donate
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
