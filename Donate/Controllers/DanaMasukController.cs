﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class DanaMasukController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DanaMasuk
        public ActionResult Index(int? RekeningId, string Start, string End)
        {
            var mutasi = db.Mutasi.Where(m => m.DonaturId != null).Include(m => m.Donatur).Include(m => m.Rekening).Include(m => m.Siswa);

            if (RekeningId != null)
                mutasi = mutasi.Where(m => m.RekeningId == RekeningId);

            if (!string.IsNullOrEmpty(Start))
            {
                DateTime StartDate = Convert.ToDateTime(Start);
                mutasi = mutasi.Where(w => DbFunctions.TruncateTime(w.LastModified) >= StartDate);

                if (!string.IsNullOrEmpty(End))
                {
                    DateTime EndDate = Convert.ToDateTime(End);
                    mutasi = mutasi.Where(w => DbFunctions.TruncateTime(w.LastModified) <= EndDate);
                }
            }

            ViewBag.RekeningId = new SelectList(db.Rekening.Select(r => new
            {
                Value = r.Id,
                Text = r.Bank + " - " + r.Nomor
            }), "Value", "Text", RekeningId);

            return View(mutasi);
        }

        // GET: DanaMasuk/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mutasi mutasi = db.Mutasi.Find(id);
            if (mutasi == null)
            {
                return HttpNotFound();
            }
            return View(mutasi);
        }

        // GET: DanaMasuk/Create
        public ActionResult Create()
        {
            ViewBag.DonaturId = new SelectList(db.DataDiri.Where(d => d.IsDonatur == true), "Id", "Nama");
            ViewBag.RekeningId = new SelectList(db.Rekening, "Id", "Bank");
            return View();
        }

        // POST: DanaMasuk/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Saldo,Debit,Kredit,Keterangan,ModifiedBy,LastModified,DonaturId,RekeningId")] Mutasi mutasi, string SKredit)
        {
            if (!string.IsNullOrEmpty(SKredit))
            {
                var lastSaldo = db.Mutasi.Where(m => m.RekeningId == mutasi.RekeningId).OrderByDescending(m => m.LastModified).Select(m => m.Saldo).FirstOrDefault();
                mutasi.Kredit = Convert.ToDecimal(ReplaceAll(",", "", SKredit));
                mutasi.Saldo = lastSaldo + mutasi.Kredit;
                mutasi.ModifiedBy = User.Identity.Name;
                mutasi.LastModified = DateTime.Now;

                if (ModelState.IsValid)
                {
                    mutasi.Id = Guid.NewGuid();
                    db.Mutasi.Add(mutasi);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ViewBag.vKredit = "not valid";
            }

            ViewBag.DonaturId = new SelectList(db.DataDiri.Where(d => d.IsDonatur == true), "Id", "Nama", mutasi.DonaturId);
            ViewBag.RekeningId = new SelectList(db.Rekening, "Id", "Bank", mutasi.RekeningId);
            return View(mutasi);
        }

        #region[replace coma in curency]
        public string ReplaceAll(string find, string replace, string str)
        {
            while (str.IndexOf(find) > -1)
            {
                str = str.Replace(find, replace);
            }
            return str;
        }
        #endregion

        // GET: DanaMasuk/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mutasi mutasi = db.Mutasi.Find(id);
            if (mutasi == null)
            {
                return HttpNotFound();
            }
            ViewBag.DonaturId = new SelectList(db.DataDiri, "Id", "Nama", mutasi.DonaturId);
            ViewBag.RekeningId = new SelectList(db.Rekening, "Id", "Bank", mutasi.RekeningId);
            ViewBag.SiswaId = new SelectList(db.DataDiri, "Id", "Nama", mutasi.SiswaId);
            return View(mutasi);
        }

        // POST: DanaMasuk/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Saldo,Debit,Kredit,Keterangan,ModifiedBy,LastModified,DonaturId,SiswaId,RekeningId")] Mutasi mutasi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mutasi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DonaturId = new SelectList(db.DataDiri, "Id", "Nama", mutasi.DonaturId);
            ViewBag.RekeningId = new SelectList(db.Rekening, "Id", "Bank", mutasi.RekeningId);
            ViewBag.SiswaId = new SelectList(db.DataDiri, "Id", "Nama", mutasi.SiswaId);
            return View(mutasi);
        }

        // GET: DanaMasuk/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mutasi mutasi = db.Mutasi.Find(id);
            if (mutasi == null)
            {
                return HttpNotFound();
            }
            return View(mutasi);
        }

        // POST: DanaMasuk/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Mutasi mutasi = db.Mutasi.Find(id);
            db.Mutasi.Remove(mutasi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
