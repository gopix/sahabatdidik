﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class PesanController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Pesan
        public ActionResult Index()
        {
            var pesan = db.Pesan.Include(p => p.Donatur);
            return View(pesan.ToList());
        }

        // POST: Pesan/Read
        [HttpPost]
        public ActionResult Read(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pesan pesan = db.Pesan.Find(id);
            if (pesan == null)
            {
                return HttpNotFound();
            }
            else
            {
                pesan.IsReaded = true;
                db.Entry(pesan).State = EntityState.Modified;
                db.SaveChanges();
            }
            return PartialView("_Read", pesan);
        }

        // GET: Pesan/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pesan pesan = db.Pesan.Find(id);
            if (pesan == null)
            {
                return HttpNotFound();
            }
            return View(pesan);
        }

        // GET: Pesan/Create
        public ActionResult Create()
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"].ToString();
            }
            return View();
        }

        // POST: Pesan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GuestName,IsiPesan,IsReaded,LastModified,DonaturId")] Pesan pesan, string Email)
        {
            var donatur = db.DataDiri.Where(d => d.Email == Email && d.IsDonatur == true).FirstOrDefault();
            if (donatur != null)
            {
                pesan.DonaturId = donatur.Id;
                pesan.GuestName = string.Empty;
            }
            pesan.LastModified = DateTime.Now;

            if (ModelState.IsValid)
            {
                pesan.Id = Guid.NewGuid();
                db.Pesan.Add(pesan);
                db.SaveChanges();
                TempData["message"] = "Berhasil mengirim pesan, Kami akan segera menghubungi Anda.";
                return RedirectToAction("Create");
            }

            return View(pesan);
        }

        // GET: Pesan/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pesan pesan = db.Pesan.Find(id);
            if (pesan == null)
            {
                return HttpNotFound();
            }
            ViewBag.DonaturId = new SelectList(db.DataDiri, "Id", "Nama", pesan.DonaturId);
            return View(pesan);
        }

        // POST: Pesan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IsiPesan,IsReaded,LastModified,DonaturId")] Pesan pesan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pesan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DonaturId = new SelectList(db.DataDiri, "Id", "Nama", pesan.DonaturId);
            return View(pesan);
        }

        // GET: Pesan/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pesan pesan = db.Pesan.Find(id);
            if (pesan == null)
            {
                return HttpNotFound();
            }
            return View(pesan);
        }

        // POST: Pesan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Pesan pesan = db.Pesan.Find(id);
            db.Pesan.Remove(pesan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
