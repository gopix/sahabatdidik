﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class ManageGalleryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ManageGallery
        public ActionResult Index()
        {
            if (TempData["gagalHapus"] != null)
            {
                ViewBag.gagalHapus = TempData["gagalHapus"].ToString();
            }
            if (TempData["berhasilHapus"] != null)
            {
                ViewBag.berhasilHapus = TempData["berhasilHapus"].ToString();
            }

            return View(db.Gallery.ToList());
        }

        // GET: ManageGallery/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Gallery.Find(id);
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        [Route("ManageGallery/Tampil/{id}")]
        [HttpPost]
        public ActionResult Tampil(Guid id)
        {
            var data = db.Gallery.Find(id);
            if (data != null)
            {
                if (data.IsActive)
                    data.IsActive = false;
                else
                    data.IsActive = true;

                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        // GET: ManageGallery/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManageGallery/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,ImageName,Image,LastModified,IsActive")] Gallery gallery)
        {
            string path = Server.MapPath("~/ImageGallery/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var NameImage = "";

            if (gallery.Image != null)
            {
                #region[extension validation]
                var listAllowExt = new List<string> { "jpg", "jpeg", "png" };
                var fileExt = Path.GetExtension(gallery.Image.FileName).Substring(1);
                if (!listAllowExt.Contains(fileExt, StringComparer.OrdinalIgnoreCase))
                    ModelState.AddModelError("Image", string.Format("hanya diijinkan file gambar dengan type {0}.", String.Join(", ", listAllowExt)));
                #endregion

                #region[save image]
                NameImage = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(gallery.Image.FileName));
                WebImage img = new WebImage(gallery.Image.InputStream);
                img.Save(path + NameImage);
                #endregion
            }
            else
            {
                ModelState.AddModelError("Image", "Gambar harus diupload");
            }

            gallery.IsActive = true;
            gallery.ImageName = NameImage;
            gallery.LastModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                gallery.Id = Guid.NewGuid();
                db.Gallery.Add(gallery);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            #region[remove image when fail save data]
            string fullPath = path + NameImage;
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            #endregion

            return View(gallery);
        }

        // GET: ManageGallery/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Gallery.Find(id);
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // POST: ManageGallery/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,ImageName,Image,LastModified,IsActive")] Gallery gallery)
        {
            string path = Server.MapPath("~/ImageGallery/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var NameImage = "";

            if (gallery.Image != null)
            {
                #region[remove old Image]
                string oldImage = path + gallery.ImageName;
                if (System.IO.File.Exists(oldImage))
                {
                    System.IO.File.Delete(oldImage);
                }
                #endregion

                #region[extension validation]
                var listAllowExt = new List<string> { "jpg", "jpeg", "png" };
                var fileExt = Path.GetExtension(gallery.Image.FileName).Substring(1);
                if (!listAllowExt.Contains(fileExt, StringComparer.OrdinalIgnoreCase))
                    ModelState.AddModelError("Image", string.Format("hanya diijinkan file gambar dengan type {0}.", String.Join(", ", listAllowExt)));
                #endregion


                #region[save image]
                NameImage = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(gallery.Image.FileName));
                WebImage img = new WebImage(gallery.Image.InputStream);
                img.Save(path + NameImage);
                #endregion

                gallery.ImageName = NameImage;
            }

            gallery.LastModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(gallery).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            #region[remove image when fail save data]
            string fullPath = path + NameImage;
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            #endregion

            return View(gallery);
        }

        //// GET: ManageGallery/Delete/5
        //public ActionResult Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Gallery gallery = db.Gallery.Find(id);
        //    if (gallery == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(gallery);
        //}

        //// POST: ManageGallery/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            string path = Server.MapPath("~/ImageGallery/");

            Gallery gallery = db.Gallery.Find(id);
            string oldImage = path + gallery.ImageName;
            db.Gallery.Remove(gallery);
            try
            {
                db.SaveChanges();
                if (System.IO.File.Exists(oldImage))
                {
                    System.IO.File.Delete(oldImage);
                }
                TempData["berhasilHapus"] = "Berhasil Hapus Data.";
            }
            catch (DbUpdateException)
            {
                TempData["gagalHapus"] = "Gagal Hapus, Data sudah digunakan";
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
