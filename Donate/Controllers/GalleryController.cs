﻿using Donate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Donate.Controllers
{
    public class GalleryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Gallery
        public ActionResult Index()
        {
            var gallery = db.Gallery.Where(w => w.IsActive);

            return View(gallery.ToList());
        }
    }
}