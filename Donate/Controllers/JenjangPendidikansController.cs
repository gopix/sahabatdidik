﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class JenjangPendidikansController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: JenjangPendidikans
        public ActionResult Index()
        {
            if (TempData["gagalHapus"] != null)
            {
                ViewBag.gagalHapus = TempData["gagalHapus"].ToString();
            }
            if (TempData["berhasilHapus"] != null)
            {
                ViewBag.berhasilHapus = TempData["berhasilHapus"].ToString();
            }

            return View(db.JenjangPendidikan.ToList());
        }

        // GET: JenjangPendidikans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JenjangPendidikan jenjangPendidikan = db.JenjangPendidikan.Find(id);
            if (jenjangPendidikan == null)
            {
                return HttpNotFound();
            }
            return View(jenjangPendidikan);
        }

        // GET: JenjangPendidikans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JenjangPendidikans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KodeJenjang,Jenjang,ModifiedBy,LastModified")] JenjangPendidikan jenjangPendidikan)
        {
            if (ModelState.IsValid)
            {
                db.JenjangPendidikan.Add(jenjangPendidikan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(jenjangPendidikan);
        }

        // GET: JenjangPendidikans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JenjangPendidikan jenjangPendidikan = db.JenjangPendidikan.Find(id);
            if (jenjangPendidikan == null)
            {
                return HttpNotFound();
            }
            return View(jenjangPendidikan);
        }

        // POST: JenjangPendidikans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KodeJenjang,Jenjang,ModifiedBy,LastModified")] JenjangPendidikan jenjangPendidikan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jenjangPendidikan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jenjangPendidikan);
        }

        //// GET: JenjangPendidikans/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    JenjangPendidikan jenjangPendidikan = db.JenjangPendidikan.Find(id);
        //    if (jenjangPendidikan == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(jenjangPendidikan);
        //}

        //// POST: JenjangPendidikans/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JenjangPendidikan jenjangPendidikan = db.JenjangPendidikan.Find(id);
            db.JenjangPendidikan.Remove(jenjangPendidikan);
            try
            {
                db.SaveChanges();
                TempData["berhasilHapus"] = "Berhasil Hapus Data.";
            }
            catch (DbUpdateException)
            {
                TempData["gagalHapus"] = "Gagal Hapus, Data sudah digunakan";
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
