﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class ManageBeritaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ManageBerita
        public ActionResult Index()
        {
            if (TempData["gagalHapus"] != null)
            {
                ViewBag.gagalHapus = TempData["gagalHapus"].ToString();
            }
            if (TempData["berhasilHapus"] != null)
            {
                ViewBag.berhasilHapus = TempData["berhasilHapus"].ToString();
            }

            var berita = db.Berita;

            return View(berita.ToList());
        }

        // GET: ManageBerita/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Berita berita = db.Berita.Find(id);
            if (berita == null)
            {
                return HttpNotFound();
            }
            return View(berita);
        }

        [Route("ManageBerita/Tampil/{id}")]
        [HttpPost]
        public ActionResult Tampil(Guid id)
        {
            var data = db.Berita.Find(id);
            if (data != null)
            {
                if (data.IsActive)
                    data.IsActive = false;
                else
                    data.IsActive = true;

                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        // GET: ManageBerita/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManageBerita/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,Title,Ikhtisar,Content,ImageName,Image,IsActive,LastModified")] Berita berita)
        {
            string path = Server.MapPath("~/ImageBerita/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var NameImage = "";

            if (berita.Image != null)
            {
                #region[extension validation]
                var listAllowExt = new List<string> { "jpg", "jpeg", "png" };
                var fileExt = Path.GetExtension(berita.Image.FileName).Substring(1);
                if(!listAllowExt.Contains(fileExt, StringComparer.OrdinalIgnoreCase))
                    ModelState.AddModelError("Image", string.Format("hanya diijinkan file gambar dengan type {0}.", String.Join(", ", listAllowExt)));
                #endregion

                #region[size validation]
                bool isValidSize = true;
                var vimg = new Bitmap(berita.Image.InputStream);
                if (vimg.Width < 790)
                    isValidSize = false;
                if(vimg.Height < 400)
                    isValidSize = false;

                if(!isValidSize)
                    ModelState.AddModelError("Image", "Gambar yang diijinkan minimal 790x400");
                #endregion

                #region[save image]
                NameImage = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(berita.Image.FileName));
                WebImage img = new WebImage(berita.Image.InputStream);
                if (img.Width > 790)
                    img.Resize(790, 400, false);
                img.Save(path + NameImage);
                #endregion
            }
            else
            {
                ModelState.AddModelError("Image", "Gambar harus diupload");
            }

            berita.IsActive = true;
            berita.ImageName = NameImage;
            berita.LastModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                berita.Id = Guid.NewGuid();
                db.Berita.Add(berita);
                //db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            #region[remove image when fail save data]
            string fullPath = path + NameImage;
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            #endregion

            return View(berita);
        }

        // GET: ManageBerita/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Berita berita = db.Berita.Find(id);
            if (berita == null)
            {
                return HttpNotFound();
            }
            return View(berita);
        }

        // POST: ManageBerita/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Ikhtisar,Content,ImageName,Image,IsActive,LastModified")] Berita berita)
        {
            string path = Server.MapPath("~/ImageBerita/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var NameImage = "";

            if (berita.Image != null)
            {
                #region[remove old Image]
                string oldImage = path + berita.ImageName;
                if (System.IO.File.Exists(oldImage))
                {
                    System.IO.File.Delete(oldImage);
                }
                #endregion

                #region[extension validation]
                var listAllowExt = new List<string> { "jpg", "jpeg", "png" };
                var fileExt = Path.GetExtension(berita.Image.FileName).Substring(1);
                if (!listAllowExt.Contains(fileExt, StringComparer.OrdinalIgnoreCase))
                    ModelState.AddModelError("Image", string.Format("hanya diijinkan file gambar dengan type {0}.", String.Join(", ", listAllowExt)));
                #endregion

                #region[size validation]
                bool isValidSize = true;
                var vimg = new Bitmap(berita.Image.InputStream);
                if (vimg.Width < 790)
                    isValidSize = false;
                if (vimg.Height < 400)
                    isValidSize = false;

                if (!isValidSize)
                    ModelState.AddModelError("Image", "Gambar yang diijinkan minimal 790x400");
                #endregion

                #region[save image]
                NameImage = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(berita.Image.FileName));
                WebImage img = new WebImage(berita.Image.InputStream);
                if (img.Width > 790)
                    img.Resize(790, 400, false);
                img.Save(path + NameImage);
                #endregion

                berita.ImageName = NameImage;
            }

            berita.LastModified = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(berita).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            #region[remove image when fail save data]
            string fullPath = path + NameImage;
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            #endregion

            return View(berita);
        }

        //// GET: ManageBerita/Delete/5
        //public ActionResult Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Berita berita = db.Berita.Find(id);
        //    if (berita == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(berita);
        //}

        //// POST: ManageBerita/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            string path = Server.MapPath("~/ImageBerita/");

            Berita berita = db.Berita.Find(id);
            string oldImage = path + berita.ImageName;
            db.Berita.Remove(berita);
            try
            {
                db.SaveChanges();
                if (System.IO.File.Exists(oldImage))
                {
                    System.IO.File.Delete(oldImage);
                }
                TempData["berhasilHapus"] = "Berhasil Hapus Data.";
            }
            catch (DbUpdateException)
            {
                TempData["gagalHapus"] = "Gagal Hapus, Data sudah digunakan";
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
