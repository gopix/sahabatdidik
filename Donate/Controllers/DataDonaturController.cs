﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class DataDonaturController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DataDonatur
        public ActionResult Index()
        {
            if (TempData["gagalHapus"] != null)
            {
                ViewBag.gagalHapus = TempData["gagalHapus"].ToString();
            }
            if (TempData["berhasilHapus"] != null)
            {
                ViewBag.berhasilHapus = TempData["berhasilHapus"].ToString();
            }

            var dataDiri = db.DataDiri.Where(d => d.IsDonatur == true).Include(d => d.Agama).Include(d => d.GolonganDarah).Include(d => d.StatusNikah);

            return View(dataDiri.ToList());
        }

        // GET: DataDonatur/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataDiri dataDiri = db.DataDiri.Find(id);
            if (dataDiri == null)
            {
                return HttpNotFound();
            }
            return View(dataDiri);
        }

        // GET: DataDonatur/Create
        public ActionResult Create()
        {
            ViewBag.AgamaId = new SelectList(db.Agama, "Id", "Nama");
            ViewBag.GolonganDarahId = new SelectList(db.GolonganDarah, "Id", "Nama");
            ViewBag.StatusNikahId = new SelectList(db.StatusNikah, "Id", "Status");
            return View();
        }

        // POST: DataDonatur/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nama,GelarDepan,GelarBelakang,TempatLahir,TglLahir,JenisKelamin,Alamat,Email,NoTelp,Kewarganegaraan,NoKtp,IsDonatur,ModifiedBy,LastModified,AgamaId,StatusNikahId,GolonganDarahId")] DataDiri dataDiri)
        {
            if (ModelState.IsValid)
            {
                db.DataDiri.Add(dataDiri);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AgamaId = new SelectList(db.Agama, "Id", "Nama", dataDiri.AgamaId);
            ViewBag.GolonganDarahId = new SelectList(db.GolonganDarah, "Id", "Nama", dataDiri.GolonganDarahId);
            ViewBag.StatusNikahId = new SelectList(db.StatusNikah, "Id", "Status", dataDiri.StatusNikahId);
            return View(dataDiri);
        }

        // GET: DataDonatur/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataDiri dataDiri = db.DataDiri.Find(id);
            if (dataDiri == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgamaId = new SelectList(db.Agama, "Id", "Nama", dataDiri.AgamaId);
            ViewBag.GolonganDarahId = new SelectList(db.GolonganDarah, "Id", "Nama", dataDiri.GolonganDarahId);
            ViewBag.StatusNikahId = new SelectList(db.StatusNikah, "Id", "Status", dataDiri.StatusNikahId);
            return View(dataDiri);
        }

        // POST: DataDonatur/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nama,GelarDepan,GelarBelakang,TempatLahir,TglLahir,JenisKelamin,Alamat,Email,NoTelp,Kewarganegaraan,NoKtp,IsDonatur,ModifiedBy,LastModified,AgamaId,StatusNikahId,GolonganDarahId")] DataDiri dataDiri)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dataDiri).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgamaId = new SelectList(db.Agama, "Id", "Nama", dataDiri.AgamaId);
            ViewBag.GolonganDarahId = new SelectList(db.GolonganDarah, "Id", "Nama", dataDiri.GolonganDarahId);
            ViewBag.StatusNikahId = new SelectList(db.StatusNikah, "Id", "Status", dataDiri.StatusNikahId);
            return View(dataDiri);
        }

        //// GET: DataDonatur/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    DataDiri dataDiri = db.DataDiri.Find(id);
        //    if (dataDiri == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(dataDiri);
        //}

        //// POST: DataDonatur/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DataDiri dataDiri = db.DataDiri.Find(id);
            db.DataDiri.Remove(dataDiri);
            try
            {
                db.SaveChanges();
                TempData["berhasilHapus"] = "Berhasil Hapus Data.";
            }
            catch (DbUpdateException)
            {
                TempData["gagalHapus"] = "Gagal Hapus, Data sudah digunakan";
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
