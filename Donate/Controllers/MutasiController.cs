﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Donate.Models;

namespace Donate.Controllers
{
    public class MutasiController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Mutasi
        public ActionResult Index(int? RekeningId, string Start, string End)
        {
            if (RekeningId == null)
                RekeningId = 1;

            var mutasi = db.Mutasi.Where(m => m.RekeningId == RekeningId).Include(m => m.Donatur).Include(m => m.Rekening).Include(m => m.Siswa);

            if (!string.IsNullOrEmpty(Start))
            {
                DateTime StartDate = Convert.ToDateTime(Start);
                mutasi = mutasi.Where(w => DbFunctions.TruncateTime(w.LastModified) >= StartDate);

                if (!string.IsNullOrEmpty(End))
                {
                    DateTime EndDate = Convert.ToDateTime(End);
                    mutasi = mutasi.Where(w => DbFunctions.TruncateTime(w.LastModified) <= EndDate);
                }
            }

            ViewBag.RekeningId = new SelectList(db.Rekening.Select(r => new
            {
                Value = r.Id,
                Text = r.Bank + " - " + r.Nomor
            }), "Value", "Text", RekeningId);

            return View(mutasi.ToList());
        }

        // GET: Mutasi/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mutasi mutasi = db.Mutasi.Find(id);
            if (mutasi == null)
            {
                return HttpNotFound();
            }
            return View(mutasi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
