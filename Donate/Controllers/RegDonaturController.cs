﻿using Donate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Donate.Controllers
{
    public class RegDonaturController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RegDonatur
        public ActionResult Index()
        {
            return View();
        }

        // GET: DataDonatur/Create
        public ActionResult Create()
        {
            if (TempData["message"] != null)
            {
                ViewBag.message = TempData["message"].ToString();
            }
            if (TempData["ExsistEmail"] != null)
            {
                ViewBag.exsistEmail = TempData["ExsistEmail"].ToString();
            }
            return View();
        }

        // POST: DataDonatur/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DataDiri dataDiri, string Pesan)
        {
            var checkExsistEmail = db.DataDiri.Where(d => d.Email == dataDiri.Email).Count();
            if (checkExsistEmail == 0)
            {
                dataDiri.IsDonatur = true;
                dataDiri.LastModified = DateTime.Now;

                if (ModelState.IsValid)
                {
                    db.DataDiri.Add(dataDiri);
                    var donaturId = db.SaveChanges();

                    #region[simpan pesan]
                    Pesan pesan = new Pesan
                    {
                        DonaturId = donaturId,
                        IsiPesan = Pesan,
                        LastModified = DateTime.Now
                    };
                    db.Pesan.Add(pesan);
                    db.SaveChanges();
                    #endregion

                    TempData["message"] = "Data Berhasil Disimpan, Kami akan segera menghubungi Anda.";
                    return RedirectToAction("Create");
                }
            }
            else
            {
                TempData["ExsistEmail"] = "Anda sudah terdaftar dalam sistem kami.";
                return RedirectToAction("Create");
            }

            ViewBag.AgamaId = new SelectList(db.Agama, "Id", "Nama", dataDiri.AgamaId);
            ViewBag.GolonganDarahId = new SelectList(db.GolonganDarah, "Id", "Nama", dataDiri.GolonganDarahId);
            ViewBag.StatusNikahId = new SelectList(db.StatusNikah, "Id", "Status", dataDiri.StatusNikahId);
            return View(dataDiri);
        }
    }
}