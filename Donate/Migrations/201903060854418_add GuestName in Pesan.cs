namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGuestNameinPesan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pesan", "GuestName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pesan", "GuestName");
        }
    }
}
