namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableBerita : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Berita",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(maxLength: 1000),
                        ImageName = c.Guid(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Berita");
        }
    }
}
