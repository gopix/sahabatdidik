namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAllTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agama",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nama = c.String(),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataDiri",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nama = c.String(nullable: false),
                        GelarDepan = c.String(),
                        GelarBelakang = c.String(),
                        TempatLahir = c.String(),
                        TglLahir = c.DateTime(),
                        JenisKelamin = c.String(),
                        Alamat = c.String(),
                        Email = c.String(nullable: false),
                        NoTelp = c.String(nullable: false),
                        Kewarganegaraan = c.String(),
                        NoKtp = c.String(),
                        IsDonatur = c.Boolean(nullable: false),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        AgamaId = c.Int(),
                        StatusNikahId = c.Int(),
                        GolonganDarahId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agama", t => t.AgamaId)
                .ForeignKey("dbo.GolonganDarah", t => t.GolonganDarahId)
                .ForeignKey("dbo.StatusNikah", t => t.StatusNikahId)
                .Index(t => t.AgamaId)
                .Index(t => t.StatusNikahId)
                .Index(t => t.GolonganDarahId);
            
            CreateTable(
                "dbo.GolonganDarah",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nama = c.String(),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PendidikanFormal",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DataDiriId = c.Int(nullable: false),
                        JenjangPendidikanId = c.Int(nullable: false),
                        NamaSekolah = c.String(),
                        Jurusan = c.String(),
                        Konsentrasi = c.String(),
                        Akreditasi = c.String(),
                        TahunMasuk = c.Int(nullable: false),
                        TahunLulus = c.Int(nullable: false),
                        AlamatSekolah = c.String(),
                        NilaiAkhir = c.String(),
                        NoIjazah = c.String(),
                        NoInduk = c.String(),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataDiri", t => t.DataDiriId, cascadeDelete: false)
                .ForeignKey("dbo.JenjangPendidikan", t => t.JenjangPendidikanId, cascadeDelete: false)
                .Index(t => t.DataDiriId)
                .Index(t => t.JenjangPendidikanId);
            
            CreateTable(
                "dbo.JenjangPendidikan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        KodeJenjang = c.String(),
                        Jenjang = c.String(),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StatusNikah",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Mutasi",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Saldo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Kredit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Keterangan = c.String(nullable: false),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        DonaturId = c.Int(),
                        SiswaId = c.Int(),
                        RekeningId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataDiri", t => t.DonaturId)
                .ForeignKey("dbo.Rekening", t => t.RekeningId, cascadeDelete: false)
                .ForeignKey("dbo.DataDiri", t => t.SiswaId)
                .Index(t => t.DonaturId)
                .Index(t => t.SiswaId)
                .Index(t => t.RekeningId);
            
            CreateTable(
                "dbo.Rekening",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bank = c.String(nullable: false),
                        Nomor = c.String(nullable: false),
                        AtasNama = c.String(nullable: false),
                        ModifiedBy = c.String(),
                        LastModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pesan",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsiPesan = c.String(),
                        IsReaded = c.Boolean(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        DonaturId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataDiri", t => t.DonaturId)
                .Index(t => t.DonaturId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pesan", "DonaturId", "dbo.DataDiri");
            DropForeignKey("dbo.Mutasi", "SiswaId", "dbo.DataDiri");
            DropForeignKey("dbo.Mutasi", "RekeningId", "dbo.Rekening");
            DropForeignKey("dbo.Mutasi", "DonaturId", "dbo.DataDiri");
            DropForeignKey("dbo.DataDiri", "StatusNikahId", "dbo.StatusNikah");
            DropForeignKey("dbo.PendidikanFormal", "JenjangPendidikanId", "dbo.JenjangPendidikan");
            DropForeignKey("dbo.PendidikanFormal", "DataDiriId", "dbo.DataDiri");
            DropForeignKey("dbo.DataDiri", "GolonganDarahId", "dbo.GolonganDarah");
            DropForeignKey("dbo.DataDiri", "AgamaId", "dbo.Agama");
            DropIndex("dbo.Pesan", new[] { "DonaturId" });
            DropIndex("dbo.Mutasi", new[] { "RekeningId" });
            DropIndex("dbo.Mutasi", new[] { "SiswaId" });
            DropIndex("dbo.Mutasi", new[] { "DonaturId" });
            DropIndex("dbo.PendidikanFormal", new[] { "JenjangPendidikanId" });
            DropIndex("dbo.PendidikanFormal", new[] { "DataDiriId" });
            DropIndex("dbo.DataDiri", new[] { "GolonganDarahId" });
            DropIndex("dbo.DataDiri", new[] { "StatusNikahId" });
            DropIndex("dbo.DataDiri", new[] { "AgamaId" });
            DropTable("dbo.Pesan");
            DropTable("dbo.Rekening");
            DropTable("dbo.Mutasi");
            DropTable("dbo.StatusNikah");
            DropTable("dbo.JenjangPendidikan");
            DropTable("dbo.PendidikanFormal");
            DropTable("dbo.GolonganDarah");
            DropTable("dbo.DataDiri");
            DropTable("dbo.Agama");
        }
    }
}
