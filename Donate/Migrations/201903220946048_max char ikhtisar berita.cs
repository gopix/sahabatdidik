namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class maxcharikhtisarberita : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Berita", "Ikhtisar", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Berita", "Ikhtisar", c => c.String(nullable: false));
        }
    }
}
