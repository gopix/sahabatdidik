namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetableberita : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Berita", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AlterColumn("dbo.Berita", "ImageName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Berita", "ImageName", c => c.Guid(nullable: false));
            DropColumn("dbo.Berita", "IsActive");
        }
    }
}
