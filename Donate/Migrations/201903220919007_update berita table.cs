namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateberitatable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Berita", "Ikhtisar", c => c.String(nullable: false));
            AlterColumn("dbo.Berita", "Content", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Berita", "Content", c => c.String(maxLength: 1000));
            DropColumn("dbo.Berita", "Ikhtisar");
        }
    }
}
