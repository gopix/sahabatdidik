namespace Donate.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGalleryTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gallery",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        ImageName = c.String(),
                        LastModified = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Gallery");
        }
    }
}
