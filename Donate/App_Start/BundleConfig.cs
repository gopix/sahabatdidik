﻿using System.Web;
using System.Web.Optimization;

namespace Donate
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/Backend/js").Include(
                        "~/Content/Backend/js/jquery-1.10.2.min.js",
                        "~/Content/Backend/js/jquery-migrate-1.0.0.min.js",
                        "~/Content/Backend/js/jquery-ui-1.10.0.custom.min.js",
                        "~/Content/Backend/js/bootstrap.min.js",
                        "~/Content/Backend/js/fullcalendar.min.js",
                        "~/Content/Backend/js/jquery.dataTables.min.js",
                        "~/Content/Backend/js/jquery.chosen.min.js",
                        "~/Content/Backend/js/jquery.uniform.min.js",
                        "~/Content/Backend/js/jquery.cleditor.min.js",
                        "~/Content/Backend/js/jquery.elfinder.min.js",
                        "~/Content/Backend/js/jquery.raty.min.js",
                        "~/Content/Backend/js/jquery.uploadify-3.1.min.js",
                        "~/Content/Backend/js/jquery.masonry.min.js",
                        "~/Content/Backend/js/jquery.sparkline.min.js",
                        "~/Content/Backend/js/sweetalert2.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Backend/css").Include(
                      "~/Content/Backend/css/bootstrap.min.css",
                      "~/Content/Backend/css/bootstrap-responsive.min.css",
                      "~/Content/Backend/css/style.css",
                      "~/Content/Backend/css/style-responsive.css",
                      "~/Content/Backend/css/sweetalert2.css"
                      ));


            BundleTable.EnableOptimizations = false;
        }
    }
}
